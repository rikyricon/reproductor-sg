# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'sinergia.ui'
#
# Created by: PyQt5 UI code generator 5.15.2
#
# WARNING: Any manual changes made to this file will be lost when pyuic5 is
# run again.  Do not edit this file unless you know what you are doing.


from PyQt5 import QtCore, QtGui, QtWidgets
from src.img import iconos

class Ui_Sinergia(object):
    def setupUi(self, Sinergia):
        Sinergia.setObjectName("Sinergia")
        Sinergia.resize(929, 611)
        self.wgtCentral = QtWidgets.QWidget(Sinergia)
        self.wgtCentral.setObjectName("wgtCentral")
        self.grCentral = QtWidgets.QGridLayout(self.wgtCentral)
        self.grCentral.setContentsMargins(0, 0, 0, 0)
        self.grCentral.setSpacing(0)
        self.grCentral.setObjectName("grCentral")
        self.frFondo = QtWidgets.QFrame(self.wgtCentral)
        self.frFondo.setFrameShape(QtWidgets.QFrame.StyledPanel)
        self.frFondo.setFrameShadow(QtWidgets.QFrame.Raised)
        self.frFondo.setObjectName("frFondo")
        self.vh1 = QtWidgets.QVBoxLayout(self.frFondo)
        self.vh1.setContentsMargins(0, 0, 0, 0)
        self.vh1.setSpacing(0)
        self.vh1.setObjectName("vh1")
        self.frMid = QtWidgets.QFrame(self.frFondo)
        self.frMid.setFrameShape(QtWidgets.QFrame.StyledPanel)
        self.frMid.setFrameShadow(QtWidgets.QFrame.Raised)
        self.frMid.setObjectName("frMid")
        self.vh1.addWidget(self.frMid)
        self.vh2 = QtWidgets.QVBoxLayout()
        self.vh2.setSpacing(0)
        self.vh2.setObjectName("vh2")
        self.frBarra = QtWidgets.QFrame(self.frFondo)
        self.frBarra.setMaximumSize(QtCore.QSize(16777215, 52))
        self.frBarra.setFrameShape(QtWidgets.QFrame.Box)
        self.frBarra.setFrameShadow(QtWidgets.QFrame.Sunken)
        self.frBarra.setObjectName("frBarra")
        self.grBarra = QtWidgets.QGridLayout(self.frBarra)
        self.grBarra.setContentsMargins(9, 4, -1, 4)
        self.grBarra.setObjectName("grBarra")

        self.btIcono = QtWidgets.QPushButton(self.frBarra)
        self.btIcono.setMaximumSize(QtCore.QSize(30, 30))
        self.btIcono.setCursor(QtGui.QCursor(QtCore.Qt.PointingHandCursor))
        icon = QtGui.QIcon()
        icon.addPixmap(QtGui.QPixmap(":/logo.jpg"), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        self.btIcono.setIcon(icon)
        self.btIcono.setIconSize(QtCore.QSize(30, 30))
        self.btIcono.setObjectName("btIcono")
        self.btIcono.setFlat(True)
        self.grBarra.addWidget(self.btIcono, 0, 0, 1, 1)

        self.btFix = QtWidgets.QPushButton(self.frBarra)
        self.btFix.setMinimumSize(QtCore.QSize(30, 0))
        self.btFix.setMaximumSize(QtCore.QSize(30, 30))
        self.btFix.setCursor(QtGui.QCursor(QtCore.Qt.PointingHandCursor))
        icon = QtGui.QIcon()
        icon.addPixmap(QtGui.QPixmap(":/StayOnTop.svg"), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        self.btFix.setIcon(icon)
        self.btFix.setIconSize(QtCore.QSize(34, 34))
        self.btFix.setFlat(True)
        self.btFix.setObjectName("btFix")
        self.grBarra.addWidget(self.btFix, 0, 1, 1, 1)
        self.lbDatos = QtWidgets.QLabel(self.frBarra)
        # self.lbDatos.setFrameShape(QtWidgets.QFrame.Box)
        self.lbDatos.setIndent(7)
        self.lbDatos.setObjectName("lbDatos")
        self.grBarra.addWidget(self.lbDatos, 0, 3, 1, 1)
        self.btClx = QtWidgets.QPushButton(self.frBarra)
        self.btClx.setMinimumSize(QtCore.QSize(30, 0))
        self.btClx.setMaximumSize(QtCore.QSize(30, 30))
        self.btClx.setCursor(QtGui.QCursor(QtCore.Qt.PointingHandCursor))
        self.btClx.setText("")
        icon1 = QtGui.QIcon()
        icon1.addPixmap(QtGui.QPixmap(":/Close.svg"), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        self.btClx.setIcon(icon1)
        self.btClx.setIconSize(QtCore.QSize(25, 25))
        self.btClx.setFlat(True)
        self.btClx.setObjectName("btClx")
        self.grBarra.addWidget(self.btClx, 0, 6, 1, 1)
        self.btMax = QtWidgets.QPushButton(self.frBarra)
        self.btMax.setMinimumSize(QtCore.QSize(30, 0))
        self.btMax.setMaximumSize(QtCore.QSize(30, 30))
        self.btMax.setCursor(QtGui.QCursor(QtCore.Qt.PointingHandCursor))
        self.btMax.setText("")
        icon2 = QtGui.QIcon()
        icon2.addPixmap(QtGui.QPixmap(":/Maximize.svg"), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        self.btMax.setIcon(icon2)
        self.btMax.setIconSize(QtCore.QSize(25, 25))
        self.btMax.setFlat(True)
        self.btMax.setObjectName("btMax")
        self.grBarra.addWidget(self.btMax, 0, 5, 1, 1)
        self.btMin = QtWidgets.QPushButton(self.frBarra)
        self.btMin.setMinimumSize(QtCore.QSize(30, 0))
        self.btMin.setMaximumSize(QtCore.QSize(30, 30))
        self.btMin.setCursor(QtGui.QCursor(QtCore.Qt.PointingHandCursor))
        self.btMin.setText("")
        icon3 = QtGui.QIcon()
        icon3.addPixmap(QtGui.QPixmap(":/Minimize.svg"), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        self.btMin.setIcon(icon3)
        self.btMin.setIconSize(QtCore.QSize(25, 25))
        self.btMin.setFlat(True)
        self.btMin.setObjectName("btMin")
        self.grBarra.addWidget(self.btMin, 0, 4, 1, 1)
        self.lbTitulo = QtWidgets.QLabel(self.frBarra)
        self.lbTitulo.setMaximumSize(QtCore.QSize(120, 16777215))
        self.lbTitulo.setText("")
        self.lbTitulo.setAlignment(QtCore.Qt.AlignCenter)
        self.lbTitulo.setObjectName("lbTitulo")
        self.grBarra.addWidget(self.lbTitulo, 0, 2, 1, 1)
        self.vh2.addWidget(self.frBarra)
        self.hz1 = QtWidgets.QHBoxLayout()
        self.hz1.setSpacing(0)
        self.hz1.setObjectName("hz1")
        self.frLista = QtWidgets.QFrame(self.frFondo)
        self.frLista.setMaximumSize(QtCore.QSize(340, 16777215))
        self.frLista.setFrameShape(QtWidgets.QFrame.StyledPanel)
        self.frLista.setFrameShadow(QtWidgets.QFrame.Sunken)
        self.frLista.setObjectName("frLista")
        self.grLista = QtWidgets.QGridLayout(self.frLista)
        self.grLista.setObjectName("grLista")
        self.ltLista = QtWidgets.QListView(self.frLista)
        self.ltLista.setObjectName("ltLista")
        self.grLista.addWidget(self.ltLista, 0, 0, 1, 6)
        self.btUltimo = QtWidgets.QPushButton(self.frLista)
        self.btUltimo.setMinimumSize(QtCore.QSize(25, 25))
        self.btUltimo.setMaximumSize(QtCore.QSize(25, 25))
        self.btUltimo.setCursor(QtGui.QCursor(QtCore.Qt.PointingHandCursor))
        self.btUltimo.setFlat(True)
        self.btUltimo.setObjectName("btUltimo")
        self.grLista.addWidget(self.btUltimo, 1, 0, 1, 1)
        self.btBajar = QtWidgets.QPushButton(self.frLista)
        self.btBajar.setMinimumSize(QtCore.QSize(25, 25))
        self.btBajar.setMaximumSize(QtCore.QSize(25, 25))
        self.btBajar.setCursor(QtGui.QCursor(QtCore.Qt.PointingHandCursor))
        self.btBajar.setFlat(True)
        self.btBajar.setObjectName("btBajar")
        self.grLista.addWidget(self.btBajar, 1, 1, 1, 1)
        self.btSubir = QtWidgets.QPushButton(self.frLista)
        self.btSubir.setMinimumSize(QtCore.QSize(25, 25))
        self.btSubir.setMaximumSize(QtCore.QSize(25, 25))
        self.btSubir.setCursor(QtGui.QCursor(QtCore.Qt.PointingHandCursor))
        self.btSubir.setFlat(True)
        self.btSubir.setObjectName("btSubir")
        self.grLista.addWidget(self.btSubir, 1, 2, 1, 1)
        self.btPrimero = QtWidgets.QPushButton(self.frLista)
        self.btPrimero.setMinimumSize(QtCore.QSize(25, 25))
        self.btPrimero.setMaximumSize(QtCore.QSize(25, 25))
        self.btPrimero.setCursor(QtGui.QCursor(QtCore.Qt.PointingHandCursor))
        self.btPrimero.setFlat(True)
        self.btPrimero.setObjectName("btPrimero")
        self.grLista.addWidget(self.btPrimero, 1, 3, 1, 1)
        self.btEliminar = QtWidgets.QPushButton(self.frLista)
        self.btEliminar.setMinimumSize(QtCore.QSize(87, 31))
        self.btEliminar.setMaximumSize(QtCore.QSize(16777215, 31))
        self.btEliminar.setCursor(QtGui.QCursor(QtCore.Qt.PointingHandCursor))
        self.btEliminar.setFlat(True)
        self.btEliminar.setObjectName("btEliminar")
        self.grLista.addWidget(self.btEliminar, 1, 4, 1, 1)
        self.enBuscar = QtWidgets.QLineEdit(self.frLista)
        self.enBuscar.setMinimumSize(QtCore.QSize(0, 22))
        self.enBuscar.setMaximumSize(QtCore.QSize(16777215, 28))
        self.enBuscar.setObjectName("enBuscar")
        self.grLista.addWidget(self.enBuscar, 1, 5, 1, 1)
        self.hz1.addWidget(self.frLista)
        self.vh3 = QtWidgets.QVBoxLayout()
        self.vh3.setSpacing(0)
        self.vh3.setObjectName("vh3")
        self.frTiempo = QtWidgets.QFrame(self.frFondo)
        self.frTiempo.setMaximumSize(QtCore.QSize(16777215, 45))
        self.frTiempo.setFrameShape(QtWidgets.QFrame.StyledPanel)
        self.frTiempo.setFrameShadow(QtWidgets.QFrame.Sunken)
        self.frTiempo.setObjectName("frTiempo")
        self.grTiempo = QtWidgets.QGridLayout(self.frTiempo)
        self.grTiempo.setObjectName("grTiempo")
        self.btFull = QtWidgets.QPushButton(self.frTiempo)
        self.btFull.setMinimumSize(QtCore.QSize(30, 0))
        self.btFull.setMaximumSize(QtCore.QSize(30, 30))
        self.btFull.setCursor(QtGui.QCursor(QtCore.Qt.PointingHandCursor))
        self.btFull.setText("")
        icon4 = QtGui.QIcon()
        icon4.addPixmap(QtGui.QPixmap(":/Fullscreen.svg"), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        self.btFull.setIcon(icon4)
        self.btFull.setIconSize(QtCore.QSize(19, 19))
        self.btFull.setFlat(True)
        self.btFull.setObjectName("btFull")
        self.grTiempo.addWidget(self.btFull, 0, 0, 1, 1)
        self.lbTranscurrido = QtWidgets.QLabel(self.frTiempo)
        self.lbTranscurrido.setObjectName("lbTranscurrido")
        self.grTiempo.addWidget(self.lbTranscurrido, 0, 1, 1, 1)
        self.slTiempo = QtWidgets.QSlider(self.frTiempo)
        self.slTiempo.setCursor(QtGui.QCursor(QtCore.Qt.PointingHandCursor))
        self.slTiempo.setOrientation(QtCore.Qt.Horizontal)
        self.slTiempo.setObjectName("slTiempo")
        self.grTiempo.addWidget(self.slTiempo, 0, 2, 1, 1)
        self.lbDuracion = QtWidgets.QLabel(self.frTiempo)
        self.lbDuracion.setIndent(7)
        self.lbDuracion.setObjectName("lbDuracion")
        self.grTiempo.addWidget(self.lbDuracion, 0, 3, 1, 1)
        self.btVolumen = QtWidgets.QPushButton(self.frTiempo)
        self.btVolumen.setMinimumSize(QtCore.QSize(30, 0))
        self.btVolumen.setMaximumSize(QtCore.QSize(30, 30))
        self.btVolumen.setCursor(QtGui.QCursor(QtCore.Qt.PointingHandCursor))
        self.btVolumen.setText("")
        icon5 = QtGui.QIcon()
        icon5.addPixmap(QtGui.QPixmap(":/Speaker.svg"), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        self.btVolumen.setIcon(icon5)
        self.btVolumen.setIconSize(QtCore.QSize(19, 19))
        self.btVolumen.setFlat(True)
        self.btVolumen.setObjectName("btVolumen")
        self.grTiempo.addWidget(self.btVolumen, 0, 4, 1, 1)
        self.slVolumen = QtWidgets.QSlider(self.frTiempo)
        self.slVolumen.setMaximumSize(QtCore.QSize(120, 16777215))
        self.slVolumen.setCursor(QtGui.QCursor(QtCore.Qt.PointingHandCursor))
        self.slVolumen.setOrientation(QtCore.Qt.Horizontal)
        self.slVolumen.setObjectName("slVolumen")
        self.grTiempo.addWidget(self.slVolumen, 0, 5, 1, 1)
        self.vh3.addWidget(self.frTiempo)
        self.frRepro = QtWidgets.QFrame(self.frFondo)
        self.frRepro.setFrameShape(QtWidgets.QFrame.StyledPanel)
        self.frRepro.setFrameShadow(QtWidgets.QFrame.Sunken)
        self.frRepro.setObjectName("frRepro")
        self.grRepro = QtWidgets.QGridLayout(self.frRepro)
        self.grRepro.setContentsMargins(0, 0, 0, 0)
        self.grRepro.setObjectName("grRepro")
        self.reproductor = QtWidgets.QGridLayout()
        self.reproductor.setSizeConstraint(QtWidgets.QLayout.SetDefaultConstraint)
        self.reproductor.setSpacing(0)
        self.reproductor.setObjectName("reproductor")
        self.lbLogo = QtWidgets.QLabel(self.frRepro)
        self.lbLogo.setMinimumSize(QtCore.QSize(0, 449))
        self.lbLogo.setText("")
        self.lbLogo.setPixmap(QtGui.QPixmap("src/img/simpson.jpg"))
        self.lbLogo.setScaledContents(True)
        self.lbLogo.setObjectName("lbLogo")
        self.reproductor.addWidget(self.lbLogo, 0, 0, 1, 1)
        self.grRepro.addLayout(self.reproductor, 0, 0, 1, 1)
        self.vh3.addWidget(self.frRepro)
        self.hz2 = QtWidgets.QHBoxLayout()
        self.hz2.setSizeConstraint(QtWidgets.QLayout.SetMaximumSize)
        self.hz2.setSpacing(0)
        self.hz2.setObjectName("hz2")
        self.frControles = QtWidgets.QFrame(self.frFondo)
        self.frControles.setMinimumSize(QtCore.QSize(549, 0))
        self.frControles.setMaximumSize(QtCore.QSize(16777215, 40))
        self.frControles.setFrameShape(QtWidgets.QFrame.StyledPanel)
        self.frControles.setFrameShadow(QtWidgets.QFrame.Sunken)
        self.frControles.setObjectName("frControles")
        self.grControles = QtWidgets.QGridLayout(self.frControles)
        self.grControles.setContentsMargins(9, 5, -1, -1)
        self.grControles.setObjectName("grControles")
        self.btLista = QtWidgets.QPushButton(self.frControles)
        self.btLista.setMinimumSize(QtCore.QSize(30, 30))
        self.btLista.setMaximumSize(QtCore.QSize(30, 30))
        self.btLista.setCursor(QtGui.QCursor(QtCore.Qt.PointingHandCursor))
        self.btLista.setText("")
        icon6 = QtGui.QIcon()
        icon6.addPixmap(QtGui.QPixmap(":/ListToggle.svg"), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        self.btLista.setIcon(icon6)
        self.btLista.setIconSize(QtCore.QSize(25, 25))
        self.btLista.setFlat(True)
        self.btLista.setObjectName("btLista")
        self.grControles.addWidget(self.btLista, 0, 0, 1, 1)
        self.btMenu = QtWidgets.QPushButton(self.frControles)
        self.btMenu.setMinimumSize(QtCore.QSize(30, 30))
        self.btMenu.setMaximumSize(QtCore.QSize(30, 30))
        self.btMenu.setCursor(QtGui.QCursor(QtCore.Qt.PointingHandCursor))
        self.btMenu.setText("")
        icon7 = QtGui.QIcon()
        icon7.addPixmap(QtGui.QPixmap(":/Settings.svg"), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        self.btMenu.setIcon(icon7)
        self.btMenu.setIconSize(QtCore.QSize(25, 25))
        self.btMenu.setFlat(True)
        self.btMenu.setObjectName("btMenu")
        self.grControles.addWidget(self.btMenu, 0, 1, 1, 1)
        self.lbEspacio = QtWidgets.QLabel(self.frControles)
        self.lbEspacio.setText("")
        self.lbEspacio.setObjectName("lbEspacio")
        self.grControles.addWidget(self.lbEspacio, 0, 2, 1, 1)
        self.btPlay = QtWidgets.QPushButton(self.frControles)
        self.btPlay.setMinimumSize(QtCore.QSize(30, 30))
        self.btPlay.setMaximumSize(QtCore.QSize(30, 30))
        self.btPlay.setCursor(QtGui.QCursor(QtCore.Qt.PointingHandCursor))
        self.btPlay.setText("")
        icon8 = QtGui.QIcon()
        icon8.addPixmap(QtGui.QPixmap(":/Play.svg"), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        self.btPlay.setIcon(icon8)
        self.btPlay.setIconSize(QtCore.QSize(25, 25))
        self.btPlay.setFlat(True)
        self.btPlay.setObjectName("btPlay")
        self.grControles.addWidget(self.btPlay, 0, 3, 1, 1)
        self.btParar = QtWidgets.QPushButton(self.frControles)
        self.btParar.setMinimumSize(QtCore.QSize(30, 30))
        self.btParar.setMaximumSize(QtCore.QSize(30, 30))
        self.btParar.setCursor(QtGui.QCursor(QtCore.Qt.PointingHandCursor))
        self.btParar.setText("")
        icon9 = QtGui.QIcon()
        icon9.addPixmap(QtGui.QPixmap(":/Stop.svg"), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        self.btParar.setIcon(icon9)
        self.btParar.setIconSize(QtCore.QSize(25, 25))
        self.btParar.setFlat(True)
        self.btParar.setObjectName("btParar")
        self.grControles.addWidget(self.btParar, 0, 4, 1, 1)
        self.btAtras = QtWidgets.QPushButton(self.frControles)
        self.btAtras.setMinimumSize(QtCore.QSize(30, 30))
        self.btAtras.setMaximumSize(QtCore.QSize(30, 30))
        self.btAtras.setCursor(QtGui.QCursor(QtCore.Qt.PointingHandCursor))
        self.btAtras.setText("")
        icon10 = QtGui.QIcon()
        icon10.addPixmap(QtGui.QPixmap(":/Previous.svg"), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        self.btAtras.setIcon(icon10)
        self.btAtras.setIconSize(QtCore.QSize(35, 37))
        self.btAtras.setFlat(True)
        self.btAtras.setObjectName("btAtras")
        self.grControles.addWidget(self.btAtras, 0, 5, 1, 1)
        self.btBack = QtWidgets.QPushButton(self.frControles)
        self.btBack.setMinimumSize(QtCore.QSize(30, 30))
        self.btBack.setMaximumSize(QtCore.QSize(30, 30))
        self.btBack.setCursor(QtGui.QCursor(QtCore.Qt.PointingHandCursor))
        self.btBack.setText("")
        icon11 = QtGui.QIcon()
        icon11.addPixmap(QtGui.QPixmap(":/Backward.svg"), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        self.btBack.setIcon(icon11)
        self.btBack.setIconSize(QtCore.QSize(35, 37))
        self.btBack.setFlat(True)
        self.btBack.setObjectName("btBack")
        self.grControles.addWidget(self.btBack, 0, 6, 1, 1)
        self.btFast = QtWidgets.QPushButton(self.frControles)
        self.btFast.setMinimumSize(QtCore.QSize(30, 30))
        self.btFast.setMaximumSize(QtCore.QSize(30, 30))
        self.btFast.setCursor(QtGui.QCursor(QtCore.Qt.PointingHandCursor))
        self.btFast.setText("")
        icon12 = QtGui.QIcon()
        icon12.addPixmap(QtGui.QPixmap(":/FastForward.svg"), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        self.btFast.setIcon(icon12)
        self.btFast.setIconSize(QtCore.QSize(35, 37))
        self.btFast.setFlat(True)
        self.btFast.setObjectName("btFast")
        self.grControles.addWidget(self.btFast, 0, 7, 1, 1)
        self.btNext = QtWidgets.QPushButton(self.frControles)
        self.btNext.setMinimumSize(QtCore.QSize(30, 30))
        self.btNext.setMaximumSize(QtCore.QSize(30, 30))
        self.btNext.setCursor(QtGui.QCursor(QtCore.Qt.PointingHandCursor))
        self.btNext.setText("")
        icon13 = QtGui.QIcon()
        icon13.addPixmap(QtGui.QPixmap(":/Next.svg"), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        self.btNext.setIcon(icon13)
        self.btNext.setIconSize(QtCore.QSize(35, 34))
        self.btNext.setFlat(True)
        self.btNext.setObjectName("btNext")
        self.grControles.addWidget(self.btNext, 0, 8, 1, 1)
        self.btCargar = QtWidgets.QPushButton(self.frControles)
        self.btCargar.setMinimumSize(QtCore.QSize(30, 30))
        self.btCargar.setMaximumSize(QtCore.QSize(30, 30))
        self.btCargar.setCursor(QtGui.QCursor(QtCore.Qt.PointingHandCursor))
        self.btCargar.setText("")
        icon14 = QtGui.QIcon()
        icon14.addPixmap(QtGui.QPixmap(":/Eject.svg"), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        self.btCargar.setIcon(icon14)
        self.btCargar.setIconSize(QtCore.QSize(25, 25))
        self.btCargar.setFlat(True)
        self.btCargar.setObjectName("btCargar")
        self.grControles.addWidget(self.btCargar, 0, 9, 1, 1)
        self.hz2.addWidget(self.frControles)
        self.vh4 = QtWidgets.QVBoxLayout()
        self.vh4.setSpacing(0)
        self.vh4.setObjectName("vh4")
        self.frGrid = QtWidgets.QFrame(self.frFondo)
        self.frGrid.setMinimumSize(QtCore.QSize(12, 20))
        self.frGrid.setMaximumSize(QtCore.QSize(12, 20))
        self.frGrid.setStyleSheet("")
        self.frGrid.setFrameShape(QtWidgets.QFrame.StyledPanel)
        self.frGrid.setFrameShadow(QtWidgets.QFrame.Raised)
        self.frGrid.setObjectName("frGrid")
        self.vh4.addWidget(self.frGrid)
        self.frame = QtWidgets.QFrame(self.frFondo)
        self.frame.setMinimumSize(QtCore.QSize(12, 20))
        self.frame.setMaximumSize(QtCore.QSize(12, 20))
        self.frame.setFrameShape(QtWidgets.QFrame.StyledPanel)
        self.frame.setFrameShadow(QtWidgets.QFrame.Raised)
        self.frame.setObjectName("frame")
        self.vh4.addWidget(self.frame)
        self.hz2.addLayout(self.vh4)
        self.vh3.addLayout(self.hz2)
        self.hz1.addLayout(self.vh3)
        self.vh2.addLayout(self.hz1)
        self.vh1.addLayout(self.vh2)
        self.grCentral.addWidget(self.frFondo, 0, 0, 1, 1)
        Sinergia.setCentralWidget(self.wgtCentral)

        self.retranslateUi(Sinergia)
        QtCore.QMetaObject.connectSlotsByName(Sinergia)

    def retranslateUi(self, Sinergia):
        _translate = QtCore.QCoreApplication.translate
        Sinergia.setWindowTitle(_translate("Sinergia", "MainWindow"))
        self.lbDatos.setText(_translate("Sinergia", "ggg"))
        self.btUltimo.setText(_translate("Sinergia", "▼"))
        self.btBajar.setText(_translate("Sinergia", "▼"))
        self.btSubir.setText(_translate("Sinergia", "▲"))
        self.btPrimero.setText(_translate("Sinergia", "▲"))
        self.btEliminar.setText(_translate("Sinergia", "Eliminar"))
        self.enBuscar.setPlaceholderText(_translate("Sinergia", "Buscar ..."))
        self.lbTranscurrido.setText(_translate("Sinergia", "Tmp Trns:"))
        self.lbDuracion.setText(_translate("Sinergia", "Duracion:"))
# import iconos_rc
